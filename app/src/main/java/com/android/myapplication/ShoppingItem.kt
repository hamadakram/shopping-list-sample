package com.android.myapplication

/**
 * Created by hammad.akram on 05/06/2020.
 * hammad.akram@accenture.com
 */

class ShoppingItem {
    var title: String = ""
    val isDone: Boolean = false
}