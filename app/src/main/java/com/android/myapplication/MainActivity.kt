package com.android.myapplication

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val shoppingList = arrayListOf<ShoppingItem>()
    lateinit var adapter: ShoppingListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Set up recyclerView
        adapter = ShoppingListAdapter()
        rvShoppingList.adapter = adapter
        rvShoppingList.layoutManager = LinearLayoutManager(this)

        // Callback from adapter for long click on item
        adapter.onLongClick = {
            // Create an alert and the delete item.
        }


        btAdd.setOnClickListener {

            val title = etItem.text.toString()
            val item = ShoppingItem()
            item.title = title

            shoppingList.add(item)

            etItem.setText("")

            adapter.shoppingList = shoppingList
            adapter.notifyDataSetChanged()
        }

    }
}
