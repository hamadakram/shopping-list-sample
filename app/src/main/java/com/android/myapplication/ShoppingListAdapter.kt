package com.android.myapplication

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.shopping_item.view.*

/**
 * Created by hammad.akram on 05/06/2020.
 * hammad.akram@accenture.com
 */

class ShoppingListAdapter : RecyclerView.Adapter<ShoppingViewHolder>() {

    var shoppingList = listOf<ShoppingItem>()
    var onLongClick: ((ShoppingItem) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShoppingViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.shopping_item, parent, false)
        return ShoppingViewHolder(view)
    }

    override fun getItemCount(): Int {
        return shoppingList.size
    }

    override fun onBindViewHolder(holder: ShoppingViewHolder, position: Int) {
        val shoppingItem = shoppingList[position]
        holder.itemView.tvTitle.text = shoppingItem.title
        holder.itemView.checkBox.isChecked = shoppingItem.isDone

        holder.itemView.setOnLongClickListener {
            onLongClick?.invoke(shoppingItem)
            return@setOnLongClickListener true
        }
    }

}

class ShoppingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
}