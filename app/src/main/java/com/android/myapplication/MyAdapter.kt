package com.android.myapplication

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.shopping_item.view.*

/**
 * Created by hammad.akram on 05/06/2020.
 * hammad.akram@accenture.com
 */
class MyAdapter(private val shoppingList: ArrayList<String>): RecyclerView.Adapter<MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.shopping_item, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return shoppingList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val title = shoppingList[position]
        holder.itemView.tvTitle.text = title
    }
}

